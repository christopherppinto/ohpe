### ohpe - Ovia Health Programming exercise

## Requirements
\> PHP 7.1  
composer  

## Building
composer install  

## Running
php bin/console server:run

### Endpoints  

Only have three endpoints from the exercise completed
  
*show room data*  
GET http://127.0.0.1:8000/rooms  

*show reservations*  
GET hhttp://127.0.0.1:8000/reservations  

*show rooms available for month/day/year**  
GET http://127.0.0.1:8000/available/12/31/2018
  
  
  
#### Design

Static test data is in the ata directory, rooms.json and reservations.json.  
  
This data is brought into the web app using file_get_contents and json_decode  
rooms.json is just an array of objects to represent the rooms.
reservations.json is the bookings, but grouped by room.  

There is no persistence of the data.

This grouping by room was done to simplify things since we do not have a DB that will allow us to query by room.
  
The reservations are stored using julian dates, this approach simplified internal logic since again no database is in use.

### Approach

I used PHP 7 and symfony 4  
In symfony 4 I used the composer command to generate a skeleton symfony application. This is a very good way to get a front controller and to build RESTful APIs quickly  
To create a skeleton symfony 4 app perform the following steps:

>  
> composer self-update  
> composer create-project symfony/skeleton central  
> cd central  
> composer require server  --dev  
> https://symfonycasts.com/screencast/symfony/setup#play  
>  

This video tutorial documents the steps  
https://symfonycasts.com/screencast/symfony/setup#play


### Extending the application

Data can be persisted via file_put_contents and json_encode  
Additional rooms can be added by editing rooms.json  
BUT it would be better to add Entity classes for the Rooms, Reservations/Bookings and to add MySQL support for persistance
The room objects could have state (needs cleaning) added to them

Preventing more than one booking for a room would require a map table with a 
unique key contraint on julian date, room number, bed number.
Everytime a room is booked a simple record for each of the days between 
the arrive and depart dates would have to be created to prevent overbooking
This operation could be wrapped in a transaction for easy rollback on failure. 

Adding new endpoints is easy with Symfony and Route annotations. (See RoomController.php and the doc blocks)
 
 



