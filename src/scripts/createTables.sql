-- auto-generated definition
create table reservation
(
  id     int auto_increment
    primary key,
  room   int  not null,
  person int  not null,
  date   date not null,
  constraint id_uindex
  unique (id),
  constraint room_person_date_uindex
  unique (room, person, date)
)
  engine = InnoDB;

create index room_index
  on reservation (room);

CREATE TABLE customer
(
  id int PRIMARY KEY AUTO_INCREMENT,
  first_name varchar(128),
  last_name varchar(128),
  phone varchar(10)
);
CREATE UNIQUE INDEX customer_id_uindex ON customer (id);
CREATE INDEX first_name_index ON customer (first_name);
CREATE INDEX last_name_index ON customer (last_name);

CREATE TABLE booking
(
  booking_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  reservation_id int,
  customer_id int
);
CREATE UNIQUE INDEX customer_reservation_reservation_id_uindex ON booking (reservation_id);
CREATE UNIQUE INDEX customer_reservation_booking_id_uindex ON booking (booking_id);