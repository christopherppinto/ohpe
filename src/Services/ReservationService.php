<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 12/5/2018
 * Time: 2:32 PM
 */

namespace App\Services;


class ReservationService
{

    private function filterDates($reservations){
        $filtered = [];
        foreach($reservations as $reservation){
            $filter = clone $reservation;
            $filter->arrives = jdtogregorian($reservation->arriveDate);
            $filter->departs = jdtogregorian($reservation->departDate);
            $filtered[] = $filter;
        }
        return $filtered;
    }
    public function getRooms(){
        return json_decode(file_get_contents(__DIR__.'/../data/rooms.json'));
    }
    public function getReservations(){
        $reservations = json_decode(file_get_contents(__DIR__.'/../data/reservations.json'));
        $filtered = new \stdClass();

        $filtered->Room1 =$this->filterDates($reservations->Room1);
        $filtered->Room2 =$this->filterDates($reservations->Room2);
        $filtered->Room3 =$this->filterDates($reservations->Room3);
        $filtered->Room4 =$this->filterDates($reservations->Room4);


        return $filtered;

    }
    public function formatReservations($reservations){


    }

    /**
     * @param $jdate int julian date to check for availability
     * @param $bookings  array of bookings for a room
     * @param $room Object the room (resources)
     * @return Object|bool
     */
    public function getRoomAvailabilityForDate($jdate, $bookings, $room){

        $roomAvailable = clone $room;
            foreach ($bookings as $booking) {
                if ($jdate >= $booking->arriveDate && $jdate < $booking->departDate) {
                    if ($room->nPeople > $booking->nPeople) {
                        $roomAvailable->nPeople = $room->nPeople - $booking->nPeople;
                        $roomAvailable->nStorage = $room->nStorage - $booking->nStorage;
                        return $roomAvailable;
                    } else {
                        return false;
                    }
                }
            }
            return $roomAvailable;
    }

    /**
     *
     * @description getAvailability given a month day year, get the rooms available based on the rooms and the bookings
     *
     * @param $month
     * @param $day
     * @param $year
     * @return array
     */
    public function getAvailability($month, $day, $year){

        $jdate = gregoriantojd($month, $day, $year);
        $bookings = $this->getReservations();
        $rooms = $this->getRooms();
        $nRooms = count($rooms);
        $availableRooms = [];
        for ($roomNum=1; $roomNum <= $nRooms; $roomNum++){
            $index = $roomNum - 1;
            $key = "Room{$roomNum}";
            $available = $this->getRoomAvailabilityForDate($jdate, $bookings->$key, $rooms[$index]);
            if ($available){
                $availableRooms[] = $available;

            }
        }

        return ["date" => "{$month}/{$day}/{$year}", "availableRooms" => $availableRooms];
    }
}


