<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 12/4/2018
 * Time: 10:20 AM
 */

namespace App\Controller;



use App\Services\ReservationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class RoomController extends AbstractController
{

    /**
     * @Route("/rooms", methods={"GET", "HEAD"})
     * @return JsonResponse
     */
    public function rooms(){

        $reservationService = new ReservationService();
        try {
            $rooms = $reservationService->getRooms();
        } catch(\Exception $e){
            return new JsonResponse(['error' => 'missing rooms.json file', 'message' => $e->getMessage()]);
        }
        return new JsonResponse($rooms);
    }

    /**
     * @Route("/bookings", methods={"GET", "HEAD"})
     * @return JsonResponse
     */
    public function bookings(){

        $reservationService = new ReservationService();

        try {
            $bookings = $reservationService->getReservations();
        } catch(\Exception $e){
            return new JsonResponse(['error' => 'missing bookings.json file', 'message' => $e->getMessage()]);
        }
        return new JsonResponse($bookings);
    }

    /**
     * @Route("/available/{month}/{day}/{year}", methods={"GET", "HEAD"})
     *
     * @param $month
     * @param $day
     * @param $year
     * @return JsonResponse
     */
    public function availableForDate($month, $day, $year) {

        $reservationService = new ReservationService();
        $roomsAvailable = $reservationService->getAvailability($month, $day, $year);
        return new JsonResponse($roomsAvailable);


    }

}
